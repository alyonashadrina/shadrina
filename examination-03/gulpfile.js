'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch');

gulp.task('html:build', function () {
    gulp.src('src/*.html')
        .pipe(gulp.dest('build'));
});

gulp.task('style:build', function () {
    gulp.src('src/css/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('build/css'));
    gulp.src('src/slick/**/*')
        .pipe(gulp.dest('build/slick'));
});

gulp.task('img:build', function () {
    gulp.src('src/img/**/*')
        .pipe(gulp.dest('build/img'));
});

gulp.task('script:build', function () {
    gulp.src('src/script/**/*')
        .pipe(gulp.dest('build/script'));
});

gulp.task('default', function(){
    gulp.watch('src/**/*', ['style:build']);
    gulp.watch('src/**/*', ['html:build']);
    gulp.watch('src/**/*', ['img:build']);
    gulp.watch('src/**/*', ['script:build']);
});
