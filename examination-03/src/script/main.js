// ---- MENU ----

if ($( window ).width() < 991) {
	$('.menu__items').hide();
}
$('.menu__icon').click(function(){
	$('.menu__items').toggle();
})
$( window ).resize(function() {
	if ($( window ).width() < 991) {
		$('.menu__items').hide();
	} else {
		$('.menu__items').show();
	}
});

// ---- MAP ----

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
});
}

// ---- SLIDERS ----

$('.works-gallery').slick({
	infinite: true,
	dots: true,
});
$('.team-gallery').slick({
	infinite: true,
	slidesToShow: 3,
	responsive: [
	{
		breakpoint: 991,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 3,
			infinite: true,
			dots: true
		}
	},{
		breakpoint: 600,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 2
		}
	},
	]
});
