$(document).ready(function(){
  $('.team-container').slick({
    dots: true,
    arrows: false,
  });
});

$(document).ready(function(){
  $('.testimonials-container').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4000,
  });
});
