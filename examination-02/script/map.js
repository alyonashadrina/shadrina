var map;
function initMap() {
	var ba = {lat: 50.446913, lng: 30.5205697};

	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: ba,
	});

	var markerImage = 'http://lms.beetroot.se/images/ba_small.png';
	var markerHome = new google.maps.Marker({
		position: ba,
		map: map,
		icon: markerImage
	});
}
