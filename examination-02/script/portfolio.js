var portfolioContainer = $('.portfolio-container');
portfolioContainer.isotope({
  itemSelector: '.portfolio-item',
  masonry: {
    gutter: 10
  }

});

$('.portfolio-selection a').on("click", function(){
	var selection = $(this).text().toLowerCase();
	if (selection === 'all works') {
		portfolioContainer.isotope({ filter: '*' });
	} else {
		portfolioContainer.isotope({
			filter: '.' + selection,
		});
	}

});
